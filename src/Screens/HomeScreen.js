import React, { useEffect } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';

import LoadingIndicator from '../Components/LoadingIndicator';
import AppButton from '../Components/AppButton';
import Card from '../Components/Card';
import colors from '../Themes/colors';
import listingsApi from '../Services/listings';
// import routes from '../Navigation/routes';
import AppText from '../Components/AppText';
import useApi from '../Hooks/useApi';

function HomeScreen({ navigation }) {
  const getListingsApi = useApi(listingsApi.getListings);

  useEffect(() => {
    getListingsApi.request();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <LoadingIndicator visible={getListingsApi.loading} />
      <View style={styles.screen}>
        {getListingsApi.error && (
          <>
            <AppText>Couldn't retrieve the listings.</AppText>
            <AppButton title="Retry" onPress={getListingsApi.request} />
          </>
        )}
        <FlatList
          data={getListingsApi.data}
          keyExtractor={listing => listing.id.toString()}
          renderItem={({ item }) => (
            <Card
              title={item.title}
              subTitle={'subtitle'}
              imageUrl={item.url}
              onPress={() => console.log(`item ${item.id} is pressed`)}
              thumbnailUrl={item.thumbnailUrl}
            />
          )}
        />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 20,
    backgroundColor: colors.light,
  },
});

export default HomeScreen;
