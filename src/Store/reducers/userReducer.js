import { LOGOUT, LOGIN } from '../actions/index';

const initialState = {
  user: null,
};

export default function trackingReducer(state = initialState, action) {
  switch (action.type) {
    case LOGOUT:
      return {
        ...state,
        user: action.payload,
      };
    case LOGIN:
      return {
        ...state,
        user: action.payload,
      };
    default:
      return state;
  }
}
